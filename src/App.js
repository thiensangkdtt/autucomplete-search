import './App.css';
import axios from 'axios';
import {useState, useEffect} from 'react'
import './custom.css';

function App() {
  const [users, setUsers] = useState([])
  const [text, setText] = useState("")
  const [suggestions, setSuggestions] = useState([])

  useEffect(() => {
    const pull_users = async() => {
      const response = await axios.get("https://reqres.in/api/users")
      setUsers(response.data.data)
    }

    pull_users()
  }, [])

  const onChangeHandler = (text) => {
    let matches = []
    if (text.length > 0) {
      matches = users.filter(user => {
        const regex = new RegExp(`${text}`, "gi")
        return user.email.match(regex)
      })
    }
    setSuggestions(matches)
    setText(text)
  }

  const onSuggestionHandler = (text) => {
    setText(text)
    setSuggestions([])
  }

  return (
    <div className="container">
      <input type="text" 
      onChange={(e)=>{onChangeHandler(e.target.value)}} 
      className="col-md-12" value={text} 
      style={{marginTop: "12px"}} 
      onBlur={() => {
          setTimeout(() => {
            setSuggestions([])
          }, 1000)
        }
      }
    />
      {suggestions && suggestions.map((suggestion, i) => 
        <div key={i} className="col-md-12 justify-content-md-center suggestion" onClick={()=> {onSuggestionHandler(suggestion.email)}}>{suggestion.email}</div>
      )}
    </div>
  );
}

export default App;
